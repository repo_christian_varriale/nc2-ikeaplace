import UIKit
import SceneKit

protocol OptionsViewControllerDelegate: class {
    func objectSelected(node: SCNNode)
    func undoLastObject()
    func togglePlaneVisualization()
    func resetScene()
}

class OptionsContainerViewController: UIViewController, UINavigationControllerDelegate {
    
    private var nav: UINavigationController?
    
    var delegate: OptionsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navigationController = UINavigationController(rootViewController: rootOptionPicker())
        nav = navigationController
        
        transition(to: navigationController)
    }
    
    override func viewWillLayoutSubviews() {
        preferredContentSize = CGSize(width: 320, height: 600)
    }
    
    private func rootOptionPicker() -> UIViewController {
        let options = [
            Option(option: ShapeOption.addScene),
            Option(option: ShapeOption.togglePlane, showsDisclosureIndicator: false),
            Option(option: ShapeOption.undoLastShape, showsDisclosureIndicator: false),
            Option(option: ShapeOption.resetScene, showsDisclosureIndicator: false)
        ]
        
        let selector = OptionSelectorViewController(options: options)
        selector.optionSelectionCallback = { [unowned self] option in
            
            switch option {
            case .addScene:
                self.nav?.pushViewController(self.scenePicker(), animated: true)
            case .togglePlane:
                self.delegate?.togglePlaneVisualization()
            case .undoLastShape:
                self.delegate?.undoLastObject()
            case .resetScene:
                self.delegate?.resetScene()
            }
        }
        return selector
    }
    
    private func scenePicker() -> UIViewController {
        let resourceFolder = "models.scnassets"
        let availableScenes: [String] = {
            let modelsURL = Bundle.main.url(forResource: resourceFolder, withExtension: nil)!
            
            let fileEnumerator = FileManager().enumerator(at: modelsURL, includingPropertiesForKeys: [])!
            
            return fileEnumerator.compactMap { element in
                let url = element as! URL
                
                guard url.pathExtension == "scn" else { return nil }
                
                return url.lastPathComponent
            }
        }()
        
        let options = availableScenes.map { Option(name: $0, option: $0, showsDisclosureIndicator: false) }
        let selector = OptionSelectorViewController(options: options)
        selector.optionSelectionCallback = { [unowned self] name in
            let nameWithoutExtension = name.replacingOccurrences(of: ".scn", with: "")
            let scene = SCNScene(named: "\(resourceFolder)/\(nameWithoutExtension)/\(name)")!
            self.delegate?.objectSelected(node: scene.rootNode)
        }
        return selector
    }
    
    
}
