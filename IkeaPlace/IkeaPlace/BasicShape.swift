import UIKit

//these are the possibilities that the user can have

enum ShapeOption: String, RawRepresentable {
    
    //every options has the string that is displayed in the screen
    case addScene = "Select Object"
    case togglePlane = "Enable/Disable Plane Visualization"
    case undoLastShape = "Remove Last Object"
    case resetScene = "Reset"
}

//for addScene there the models in models.scnassets
